<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Produto::factory(40)->create();
        \App\Models\GrupoDeCidades::factory(15)->create();
        \App\Models\Cidade::factory(40)->create();
        \App\Models\Campanha::factory(10)->create();
        \App\Models\ProdutoCampanha::factory(60)->create();
        \App\Models\Desconto::factory(10)->create();
    }
}
