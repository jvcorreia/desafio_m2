<?php

namespace Database\Factories;

use App\Models\ProdutoCampanha;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProdutoCampanhaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProdutoCampanha::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'produto_id' => $this->faker->numberBetween(1, 40),
            'campanha_id' =>  $this->faker->numberBetween(1, 10)
        ];
    }
}
