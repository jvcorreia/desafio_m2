<?php

namespace Database\Factories;

use App\Models\Desconto;
use Illuminate\Database\Eloquent\Factories\Factory;

class DescontoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Desconto::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nome' => 'desconto'.$this->faker->name(),
            'porcentagem' => $this->faker->numberBetween(1, 12),
            'campanha_id'  => $this->faker->numberBetween(1, 10)
        ];
    }
}
