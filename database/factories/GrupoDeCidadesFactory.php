<?php

namespace Database\Factories;

use App\Models\GrupoDeCidades;
use Illuminate\Database\Eloquent\Factories\Factory;

class GrupoDeCidadesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = GrupoDeCidades::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nome' => $this->faker->name()
        ];
    }
}
