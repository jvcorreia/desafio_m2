<h3 align="center">Comandos para rodar o projeto:</h3>
<p align="center">php artisan migrate</p>
<p align="center">php artisan db:seed</p>
<p align="center">php artisan serve</p>
<br>
<p align="center">Banco de dados utilizado foi o Postgres</p>
<br>
<br>
<br>
<h3 align="center">Endpoints:</h3>
<p align="center">api/campanha/{id} [GET]</p>
<p align="center">X</p>
<br>
<p align="center">api/campanha/  [POST]</p>
<p align="center">'nome' => 'required|string',
                  'grupo_de_cidades_id' => 'int|required|exists:grupos_de_cidades,id',
                  'is_ativo'  => 'boolean|required'
</p>
<br>
<p align="center">api/campanha/{id}  [PUT]</p>
<p align="center">'nome' => 'string',
                  'grupo_de_cidades_id' => 'int|exists:grupos_de_cidades,id',
                  'is_ativo'  => 'boolean'
</p>
<br>
<p align="center">api/campanha/{id}  [DELETE]</p>
<p align="center">X</p>
<br>

<p align="center">api/cidade/{id} [GET]</p>
<p align="center">X</p>
<br>


<p align="center">api/cidade/  [POST]</p>
<p align="center">'nome' => 'required|string',
            'uf'   => 'required|string|min:2|max:2',
            'grupo_de_cidades_id' => 'required|int|exists:grupos_de_cidades,id'
</p>
<br>
<p align="center">api/cidade/{id}  [PUT]</p>
<p align="center">'nome' => 'string',
            'uf'   => 'string|min:2|max:2',
            'grupo_de_cidades_id' => 'int|exists:grupos_de_cidades,id'
</p>
<br>
<p align="center">api/cidade/{id}  [DELETE]</p>
<p align="center">X</p>
<br>


<p align="center">api/desconto/{id} [GET]</p>
<p align="center">X</p>
<br>
<p align="center">api/desconto/  [POST]</p>
<p align="center">'nome' => 'required|string',
            'porcentagem' => 'required|int',
            'campanha_id' => 'required|exists:campanhas,id'
</p>
<br>
<p align="center">api/desconto/{id}  [PUT]</p>
<p align="center"> 'nome' => 'string',
            'porcentagem' => 'int',
            'campanha_id' => 'exists:campanhas,id'
</p>
<br>
<p align="center">api/desconto/{id}  [DELETE]</p>
<p align="center">X</p>
<br>


<p align="center">api/grupoDeCidades/{id} [GET]</p>
<p align="center">X</p>
<br>
<p align="center">api/grupoDeCidades/  [POST]</p>
<p align="center"> 'nome' => 'required|string'
</p>
<br>
<p align="center">api/grupoDeCidades/{id}  [PUT]</p>
<p align="center"> 'nome' => 'string'
</p>
<br>
<p align="center">api/grupoDeCidades/{id}  [DELETE]</p>
<p align="center">X</p>
<br>


<p align="center">api/produto/{id} [GET]</p>
<p align="center">X</p>
<br>
<p align="center">api/produto/  [POST]</p>
<p align="center"> 'nome' => 'required|string',
            'descricao' => 'required|string',
            'preco'     => 'required|numeric'
</p>
<br>
<p align="center">api/produto/{id}  [PUT]</p>
<p align="center">'nome' => 'string',
            'descricao' => 'string',
            'preco'     => 'numeric'
</p>
<br>
<p align="center">api/produto/{id}  [DELETE]</p>
<p align="center">X</p>
<br>




