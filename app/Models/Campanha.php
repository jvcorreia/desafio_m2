<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Campanha extends Model
{
    use HasFactory;

    protected $table = 'campanhas';

    protected $fillable = [
        'nome',
        'grupo_de_cidades_id',
        'is_ativo',
    ];

    public function grupoDaCidade()
    {
        return $this->hasOne(GrupoDeCidades::class, 'id', 'grupo_de_cidades_id');
    }

    public function outraCampanhaAtiva($grupoDeCidades)
    {
        return $this->where('is_ativo', true)->where('grupo_de_cidades_id', $grupoDeCidades)->first();
    }

}
