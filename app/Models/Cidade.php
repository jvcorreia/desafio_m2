<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cidade extends Model
{
    use HasFactory;

    protected $table = 'cidades';

    protected $fillable = [
        'nome',
        'uf',
        'grupo_de_cidades_id'
    ];

    public function GrupoDeCidades()
    {
        return $this->belongsTo(GrupoDeCidades::class);
    }
}
