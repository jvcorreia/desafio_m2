<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GrupoDeCidades extends Model
{
    use HasFactory;

    protected $table = 'grupos_de_cidades';

    protected $fillable = [
        'nome'
    ];

}
