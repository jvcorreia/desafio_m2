<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Desconto extends Model
{
    use HasFactory;

    protected $table = 'descontos';

    protected $fillable = [
        'nome',
        'porcentagem',
        'campanha_id'
    ];

    public function campanha()
    {
        return $this->belongsTo(Campanha::class);
    }
}
