<?php

namespace App\Exceptions;

use App\Core\CoreException;
use Exception;

class CidadeException extends CoreException
{
    public function __construct(string $message, int $code = 400)
    {
        $this->message  = $message;
        $this->code     = $code;

        parent::__construct($message, $code);
    }
}
