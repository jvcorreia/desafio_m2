<?php

namespace App\Core;

use Exception;
use Illuminate\Support\Facades\Log;

class CoreException extends Exception
{
    public function report()
    {
        if (in_array($this->code, [500, 404])) {
            Log::error($this->message);
        } else {
            Log::debug($this->message);
        }
    }

    public function render()
    {
        return response()->json([
            'success' => false,
            'message' => $this->message,
            'trace' => $this->getTraceAsString()
        ], $this->code);
    }


}
