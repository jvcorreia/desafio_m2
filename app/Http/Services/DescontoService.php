<?php

namespace App\Http\Services;

use App\Exceptions\DescontoException;
use App\Models\Desconto;

class DescontoService
{
    private $descontoModel;

    public function __construct(Desconto $descontoModel)
    {
        $this->descontoModel = $descontoModel;
    }

    /**
     * @throws DescontoException
     */
    public function createDesconto($desconto)
    {
        $created = $this->descontoModel->create($desconto);
        if ($created) return $created;
        throw new DescontoException('O desconto não foi criado.', 500);
    }

    /**
     * @throws DescontoException
     * @throws \Throwable
     */
    public function updateDesconto($desconto, $request)
    {
        $descontoFilled = $desconto->fill($request);
        $descont = new Desconto($descontoFilled->toArray());
        if($descont->saveOrFail()) return $descontoFilled;
        throw new DescontoException('O desconto não foi atualizado.', 500);
    }
}
