<?php

namespace App\Http\Services;

use App\Exceptions\ProdutoException;
use App\Models\Produto;

class ProdutoService
{
    private $produtoModel;

    public function __construct(Produto $produtoModel)
    {
        $this->produtoModel = $produtoModel;
    }

    /**
     * @throws ProdutoException
     */
    public function createProduto($produto)
    {
        $created = $this->produtoModel->create($produto);
        if ($created) return $created;
        throw new ProdutoException('O produto não foi criado', 500);
    }

    /**
     * @throws \Throwable
     * @throws ProdutoException
     */
    public function updateProduto($produto, $request)
    {
        $produtoFilled = $produto->fill($request);
        $prod = new Produto($produtoFilled->toArray());
        if($prod->saveOrFail()) return $produtoFilled;
        throw new ProdutoException('O produto não foi atualizado', 500);;
    }
}
