<?php

namespace App\Http\Services;

use App\Exceptions\CampanhaException;
use App\Models\Campanha;
use Exception;

class CampanhaService
{
    private $campanhaModel;

    public function __construct(Campanha $campanhaModel)
    {
        $this->campanhaModel = $campanhaModel;
    }

    /**
     * @throws CampanhaException
     */
    public function createCampanha($campanha)
    {
        if ($campanha['is_ativo']) {
            $this->outraCampanhaAtiva($campanha['grupo_de_cidades_id']);
        }

        $created = $this->campanhaModel->create($campanha);
        if ($created) return $created;
        throw new CampanhaException('A campanha não foi criada', 500);
    }

    /**
     * @throws CampanhaException
     */
    public function outraCampanhaAtiva($grupoDeCidades)
    {
        if ($this->campanhaModel->outraCampanhaAtiva($grupoDeCidades)) {
            throw new CampanhaException('Já existe Campanha ativa para esse grupo de cidades.', 400);
        }
    }

    /**
     * @throws \Throwable
     * @throws CampanhaException
     */
    public function updateCampanha($campanha, $request)
    {
        if (isset($request['is_ativo'])) {
            $this->outraCampanhaAtiva($campanha['grupo_de_cidades_id']);
        }

        $campanhaFille = $campanha->fill($request);
        $camp = new Campanha($campanhaFille->toArray());
        if($camp->saveOrFail()) return $campanhaFille;
        throw new CampanhaException('A campanha não foi atualizada', 500);
    }
}
