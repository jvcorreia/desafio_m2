<?php

namespace App\Http\Services;

use App\Exceptions\CidadeException;
use App\Models\Cidade;

class CidadeService
{
    private $cidadeModel;

    public function __construct(Cidade $cidadeModel)
    {
        $this->cidadeModel = $cidadeModel;
    }

    /**
     * @throws CidadeException
     */
    public function createCidade($cidade)
    {
        $created =  $this->cidadeModel->create($cidade);
        if ($created) return $created;
        throw new CidadeException('A cidade não foi criada.', 500);
    }

    /**
     * @throws \Throwable
     * @throws CidadeException
     */
    public function updateCidade($cidade, $request)
    {
        $cidadeFilled = $cidade->fill($request);
        $cidad = new Cidade($cidadeFilled->toArray());
        if($cidad->saveOrFail()) return $cidadeFilled;
        throw new CidadeException('A cidade não foi atualizazda.', 500);
    }
}
