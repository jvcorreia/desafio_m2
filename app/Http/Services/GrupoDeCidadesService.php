<?php

namespace App\Http\Services;

use App\Exceptions\GrupoDeCidadesException;
use App\Models\GrupoDeCidades;

class GrupoDeCidadesService
{
    private $grupoDeCidadesModel;

    public function __construct(GrupoDeCidades $grupoDeCidadesModel)
    {
        $this->grupoDeCidadesModel = $grupoDeCidadesModel;
    }

    /**
     * @throws GrupoDeCidadesException
     */
    public function createGrupoDeCidades($grupoDeCidades)
    {
        $created = $this->grupoDeCidadesModel->create($grupoDeCidades);
        if ($created) return $created;
        throw new GrupoDeCidadesException('O grupo de cidades não foi criado', 500);
    }

    /**
     * @throws GrupoDeCidadesException
     * @throws \Throwable
     */
    public function updateGrupoDeCidades($grupoDeCidades, $request)
    {
        $grupoDeCidadesFilled = $grupoDeCidades->fill($request);
        $gp = new GrupoDeCidades($grupoDeCidadesFilled->toArray());
        if($gp->saveOrFail()) return $grupoDeCidadesFilled;
        throw new GrupoDeCidadesException('O grupo de cidades não foi atualizado', 500);;
    }

}
