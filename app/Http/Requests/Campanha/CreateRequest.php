<?php

namespace App\Http\Requests\Campanha;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|string',
            'grupo_de_cidades_id' => 'int|required|exists:grupos_de_cidades,id',
            'is_ativo'  => 'boolean|required'
        ];
    }
}
