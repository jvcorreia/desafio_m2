<?php

namespace App\Http\Controllers;

use App\Exceptions\GrupoDeCidadesException;
use App\Http\Requests\GrupoDeCidades\CreateRequest;
use App\Http\Requests\GrupoDeCidades\UpdateRequest;
use App\Http\Services\GrupoDeCidadesService;
use App\Models\GrupoDeCidades;
use Illuminate\Http\JsonResponse;


class GrupoDeCidadesController extends Controller
{
    private $grupoDeCidadesService;

    public function __construct(GrupoDeCidadesService $grupoDeCidadesService)
    {
        $this->grupoDeCidadesService = $grupoDeCidadesService;
    }

    public function get(GrupoDeCidades $grupoDeCidades): JsonResponse
    {
        return $this->response200($grupoDeCidades);
    }

    /**
     * @throws GrupoDeCidadesException
     */
    public function create(CreateRequest $request): JsonResponse
    {
        return $this->response200($this->grupoDeCidadesService->createGrupoDeCidades($request->validated()));
    }

    /**
     * @throws \Throwable
     */
    public function delete(GrupoDeCidades $grupoDeCidades): JsonResponse
    {
        return $this->response200($grupoDeCidades->deleteOrFail());
    }

    /**
     * @throws GrupoDeCidadesException
     * @throws \Throwable
     */
    public function update(GrupoDeCidades $grupoDeCidades, UpdateRequest $request): JsonResponse
    {
        return $this->response200($this->grupoDeCidadesService->updateGrupoDeCidades($grupoDeCidades, $request->validated()));
    }
}
