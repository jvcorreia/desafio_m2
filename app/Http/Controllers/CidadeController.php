<?php

namespace App\Http\Controllers;

use App\Exceptions\CidadeException;
use App\Http\Requests\Cidade\CreateRequest;
use App\Http\Requests\Cidade\UpdateRequest;
use App\Http\Services\CidadeService;
use App\Models\Cidade;
use Illuminate\Http\JsonResponse;



class CidadeController extends Controller
{
    private $cidadeService;

    public function __construct(CidadeService $cidadeService)
    {
        $this->cidadeService = $cidadeService;
    }

    public function get(Cidade $cidade)
    {
        return $this->response200(Cidade::with('GrupoDeCidades')->find($cidade['id']));
    }

    /**
     * @throws CidadeException
     */
    public function create(CreateRequest $request): JsonResponse
    {
        return $this->response200($this->cidadeService->createCidade($request->validated()));
    }

    /**
     * @throws \Throwable
     */
    public function delete(Cidade $cidade): JsonResponse
    {
        return $this->response200($cidade->deleteOrFail());
    }

    /**
     * @throws \Throwable
     * @throws CidadeException
     */
    public function update(Cidade $cidade, UpdateRequest $request): JsonResponse
    {
        return $this->response200($this->cidadeService->updateCidade($cidade, $request->validated()));
    }
}
