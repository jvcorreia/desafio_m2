<?php

namespace App\Http\Controllers;

use App\Exceptions\CampanhaException;
use App\Http\Requests\Campanha\CreateRequest;
use App\Http\Requests\Campanha\UpdateRequest;
use App\Http\Services\CampanhaService;
use App\Models\Campanha;
use Illuminate\Http\JsonResponse;


class CampanhaController extends Controller
{
    private $campanhaService;

    public function __construct(CampanhaService $campanhaService)
    {
        $this->campanhaService = $campanhaService;
    }

    public function get(Campanha $campanha): JsonResponse
    {
        return $this->response200(Campanha::with('grupoDaCidade')->find($campanha['id']));
    }

    /**
     * @throws CampanhaException
     */
    public function create(CreateRequest $request): JsonResponse
    {
        return $this->response200($this->campanhaService->createCampanha($request->validated()));
    }

    /**
     * @throws \Throwable
     */
    public function delete(Campanha $campanha): JsonResponse
    {
        return $this->response200($campanha->deleteOrFail());
    }

    /**
     * @throws \Throwable
     * @throws CampanhaException
     */
    public function update(Campanha $campanha, UpdateRequest $request): JsonResponse
    {
        return $this->response200($this->campanhaService->updateCampanha($campanha, $request->validated()));
    }
}
