<?php

namespace App\Http\Controllers;

use App\Exceptions\DescontoException;
use App\Http\Requests\Desconto\CreateRequest;
use App\Http\Requests\Desconto\UpdateRequest;
use App\Http\Services\DescontoService;
use App\Models\Desconto;
use Illuminate\Http\JsonResponse;


class DescontoController extends Controller
{
    private $descontoService;

    public function __construct(DescontoService $descontoService)
    {
        $this->descontoService = $descontoService;
    }

    public function get(Desconto $desconto): JsonResponse
    {
        return $this->response200(Desconto::with('campanha')->find($desconto['id']));
    }

    /**
     * @throws DescontoException
     */
    public function create(CreateRequest $request): JsonResponse
    {
        return $this->response200($this->descontoService->createDesconto($request->validated()));
    }

    /**
     * @throws \Throwable
     */
    public function delete(Desconto $desconto): JsonResponse
    {
        return $this->response200($desconto->deleteOrFail());
    }

    /**
     * @throws \Throwable
     * @throws DescontoException
     */
    public function update(Desconto $desconto, UpdateRequest $request): JsonResponse
    {
        return $this->response200($this->descontoService->updateDesconto($desconto, $request->validated()));
    }

}
