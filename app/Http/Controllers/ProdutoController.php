<?php

namespace App\Http\Controllers;

use App\Exceptions\ProdutoException;
use App\Http\Requests\Produto\CreateRequest;
use App\Http\Requests\Produto\UpdateRequest;
use App\Http\Services\ProdutoService;
use App\Models\Produto;
use Illuminate\Http\JsonResponse;


class ProdutoController extends Controller
{
    private $produtoService;

    public function __construct(ProdutoService $produtoService)
    {
        $this->produtoService = $produtoService;
    }

    public function get(Produto $produto): JsonResponse
    {
        return $this->response200($produto);
    }

    /**
     * @throws ProdutoException
     */
    public function create(CreateRequest $request): JsonResponse
    {
        return $this->response200($this->produtoService->createProduto($request->validated()));
    }

    /**
     * @throws \Throwable
     */
    public function delete(Produto $produto): JsonResponse
    {
        return $this->response200($produto->deleteOrFail());
    }

    /**
     * @throws ProdutoException
     * @throws \Throwable
     */
    public function update(Produto $produto, UpdateRequest $request): JsonResponse
    {
        return $this->response200($this->produtoService->updateProduto($produto, $request->validated()));
    }
}
