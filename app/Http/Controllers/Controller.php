<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function response200($data = [], $code = 200)
    {
        return response()->json(['success' => true, 'data' => $data], $code);
    }

    public function response400($data = [], $code = 400)
    {
        return response()->json(['success' => false, 'data' => $data], $code);
    }

    public function response500($data = [], $code = 500)
    {
        return response()->json(['success' => false, 'data' => $data], $code);
    }
}
