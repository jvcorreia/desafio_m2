<?php

use App\Http\Controllers\CampanhaController;
use Illuminate\Support\Facades\Route;


Route::get('/{campanha}', [CampanhaController::class, 'get']);
Route::post('/', [CampanhaController::class, 'create']);
Route::delete('/{campanha}', [CampanhaController::class, 'delete']);
Route::put('/{campanha}', [CampanhaController::class, 'update']);
