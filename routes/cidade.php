<?php

use App\Http\Controllers\CidadeController;
use Illuminate\Support\Facades\Route;

Route::get('/{cidade}', [CidadeController::class, 'get']);
Route::post('/', [CidadeController::class, 'create']);
Route::delete('/{cidade}', [CidadeController::class, 'delete']);
Route::put('/{cidade}', [CidadeController::class, 'update']);
