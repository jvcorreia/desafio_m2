<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('cidade')->group(base_path('routes/cidade.php'));
Route::prefix('produto')->group(base_path('routes/produto.php'));
Route::prefix('desconto')->group(base_path('routes/desconto.php'));
Route::prefix('grupoDeCidades')->group(base_path('routes/grupodecidades.php'));
Route::prefix('campanha')->group(base_path('routes/campanha.php'));

