<?php

use App\Http\Controllers\GrupoDeCidadesController;
use Illuminate\Support\Facades\Route;

Route::get('/{grupoDeCidades}', [GrupoDeCidadesController::class, 'get']);
Route::post('/', [GrupoDeCidadesController::class, 'create']);
Route::delete('/{grupoDeCidades}', [GrupoDeCidadesController::class, 'delete']);
Route::put('/{grupoDeCidades}', [GrupoDeCidadesController::class, 'update']);
