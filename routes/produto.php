<?php

use App\Http\Controllers\ProdutoController;
use Illuminate\Support\Facades\Route;

Route::get('/{produto}', [ProdutoController::class, 'get']);
Route::post('/', [ProdutoController::class, 'create']);
Route::delete('/{produto}', [ProdutoController::class, 'delete']);
Route::put('/{produto}', [ProdutoController::class, 'update']);
