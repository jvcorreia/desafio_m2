<?php

use App\Http\Controllers\DescontoController;
use Illuminate\Support\Facades\Route;


Route::get('/{desconto}', [DescontoController::class, 'get']);
Route::post('/', [DescontoController::class, 'create']);
Route::delete('/{desconto}', [DescontoController::class, 'delete']);
Route::put('/{desconto}', [DescontoController::class, 'update']);
